package com.minecraftonline.mixincraftonline.util;

import com.minecraftonline.mixincraftonline.MixincraftOnline;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryLargeChest;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityHopper;
import net.minecraft.util.EntitySelectors;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class InventoryCache {

    private final World world;
    private final BlockPos pos;
    private IInventory inventory;
    private boolean loaded = false;
    private boolean movingInventory = false;
    private AxisAlignedBB aabb = null;
    private final boolean onChunkBorder;
    private int cooldownTick = 0;

    public InventoryCache(World world, final BlockPos pos) {
        this.world = world;
        this.pos = pos;
        this.onChunkBorder = BlockUtil.isOnChunkBorder(pos);
        if (this.onChunkBorder) {
            MixincraftOnline.getInstance().addInvalidateMap(this);
        }
    }

    @Nullable
    public IInventory getOrLoad() {
        if (!loaded) {
            if (onChunkBorder) {
                if (cooldownTick++ % 5 != 0) {
                    return null;
                }
                if (!world.isBlockLoaded(pos)) {
                    return null;
                }
            }
            inventory = TileEntityHopper.getInventoryAtPosition(world, pos.getX(), pos.getY(), pos.getZ());
            if (inventory == null || !(inventory instanceof TileEntity)) {
                if (world.getBlockState(pos).getBlock().isPassable(world, pos)) {
                    movingInventory = true;
                    aabb = new AxisAlignedBB(pos);
                }
            }
            loaded = true;
            return inventory;
        }
        else if (onChunkBorder && !world.isBlockLoaded(pos)) {
            this.loaded = false;
            this.inventory = null;
            return null;
        }
        if (movingInventory) {
            // No other moving inventories that I know of. Using EntityMinecart.class should speed up lookup massively.
            // Note: This does not look for items, that is handled if we return null.
            List<Entity> list = world.getEntitiesWithinAABB(EntityMinecart.class, aabb, EntitySelectors.HAS_INVENTORY);
            if (!list.isEmpty()) {
                return (IInventory)list.get(world.rand.nextInt(list.size()));
            }
        }
        return inventory;
    }

    public void invalidate() {
        loaded = false;
    }

    public BlockPos getPos() {
        return pos;
    }

    public World getWorld() {
        return world;
    }
}
