package com.minecraftonline.mixincraftonline.mixins;

import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import javax.annotation.Nullable;
import java.util.UUID;

@Mixin(targets = "org.spongepowered.common.service.user.UserDiscoverer", remap = false)
public class DontQueryBanListForPlayersMixin {

    /**
     * @param uniqueId UUID to ignore
     * @return null, always
     * @author tyhdefu
     * @reason Don't query the banned players DB on the main thread yikerinos.
     */
    @Nullable
    @Overwrite
    private static User getFromBanlist(UUID uniqueId) {
        return null;
    }
}
