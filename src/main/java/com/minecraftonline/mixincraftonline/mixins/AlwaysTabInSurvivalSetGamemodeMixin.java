package com.minecraftonline.mixincraftonline.mixins;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.Packet;
import net.minecraft.server.management.PlayerInteractionManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(PlayerInteractionManager.class)
public abstract class AlwaysTabInSurvivalSetGamemodeMixin {

    @Shadow public EntityPlayerMP player;

    @Redirect(method = "setGameType", at = @At(value = "INVOKE", target = "Lnet/minecraft/server/management/PlayerList;sendPacketToAllPlayers(Lnet/minecraft/network/Packet;)V"))
    public void onlySendGameTypeToSelf(net.minecraft.server.management.PlayerList playerList, Packet<?> packet) {
        if (this.player == null || this.player.connection == null) {
            return;
        }
        this.player.connection.sendPacket(packet);
    }
}
