package com.minecraftonline.mixincraftonline.mixins;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.advancements.DisplayInfo;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(DisplayInfo.class)
public class IconNbtMixin {

    @Inject(method = "deserializeIcon", at = @At("RETURN"))
    private static void onReturn(JsonObject json, CallbackInfoReturnable<ItemStack> cir) {
        ItemStack itemStack = cir.getReturnValue();
        try {
            JsonElement element = json.get("nbt");
            if (element == null) {
                return;
            }
            NBTTagCompound nbt = JsonToNBT.getTagFromJson(element.getAsString());
            itemStack.setTagCompound(nbt);
        } catch (NBTException e) {
            e.printStackTrace();
        }
    }
}
