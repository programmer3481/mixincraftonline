package com.minecraftonline.mixincraftonline.mixins;

import net.minecraft.world.GameRules;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(net.minecraft.command.CommandExecuteAt.class)
public abstract class CommandExecuteAt {

    @Redirect(method = "execute", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/GameRules;getBoolean(Ljava/lang/String;)Z"))
    private boolean onGetBoolean(GameRules gameRules, String name) {
        // use the sendCommandFeedback gamerule instead of commandBlockOutput for execute response
        // as execute can often be used by things other than command blocks.
        return gameRules.getBoolean("sendCommandFeedback");
    }
}
