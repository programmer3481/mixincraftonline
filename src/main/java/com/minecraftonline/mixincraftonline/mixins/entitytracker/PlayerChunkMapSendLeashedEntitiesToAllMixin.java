package com.minecraftonline.mixincraftonline.mixins.entitytracker;

import com.minecraftonline.mixincraftonline.bridge.EntityTrackerBridge;
import net.minecraft.entity.EntityTracker;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketChunkData;
import net.minecraft.server.management.PlayerChunkMap;
import net.minecraft.server.management.PlayerChunkMapEntry;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Batch together player updates so that you only need to iterate through
 * to find the tracker chunk entry once.
 */
@Mixin(PlayerChunkMapEntry.class)
public abstract class PlayerChunkMapSendLeashedEntitiesToAllMixin {

    @Shadow @Nullable private Chunk chunk;

    @Shadow @Final private List<EntityPlayerMP> players;

    @Shadow @Final private PlayerChunkMap playerChunkMap;


    @Redirect(method = "sendToPlayers", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/EntityTracker;sendLeashedEntitiesInChunk(Lnet/minecraft/entity/player/EntityPlayerMP;Lnet/minecraft/world/chunk/Chunk;)V"))
    protected void cancelSingularSend(EntityTracker entityTracker, EntityPlayerMP player, Chunk chunkIn) {
        // Do nothing.
    }

    @Inject(method = "sendToPlayers",
            slice = @Slice(
                    from = @At(value = "INVOKE",
                            target = "Lnet/minecraft/entity/EntityTracker;sendLeashedEntitiesInChunk(Lnet/minecraft/entity/player/EntityPlayerMP;Lnet/minecraft/world/chunk/Chunk;)V")
            ),
            at = @At("RETURN"))
    public void addBatch(CallbackInfoReturnable<Boolean> cir) {
        EntityTracker entityTracker = this.playerChunkMap.getWorldServer().getEntityTracker();
        ((EntityTrackerBridge) entityTracker).sendLeashedEntitiesInChunkToAll(this.players, this.chunk);
    }
}
