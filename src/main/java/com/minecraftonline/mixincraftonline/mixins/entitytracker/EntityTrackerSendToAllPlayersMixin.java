package com.minecraftonline.mixincraftonline.mixins.entitytracker;

import com.google.common.collect.Lists;
import com.minecraftonline.mixincraftonline.bridge.EntityTrackerBridge;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityTracker;
import net.minecraft.entity.EntityTrackerEntry;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketEntityAttach;
import net.minecraft.network.play.server.SPacketSetPassengers;
import net.minecraft.world.chunk.Chunk;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.List;
import java.util.Set;

/**
 * Implement EntityTrackerBridge to send leashed/passengers
 * to players, without having to repeatedly generate the same lists
 * and iterate to find the same EntityTrackerEntry
 */
@Mixin(EntityTracker.class)
public abstract class EntityTrackerSendToAllPlayersMixin implements EntityTrackerBridge {
    @Shadow @Final private Set<EntityTrackerEntry> entries;

    @Override
    public void sendLeashedEntitiesInChunkToAll(Iterable<EntityPlayerMP> players, Chunk chunkIn) {
        List<Entity> leashedEntities = Lists.<Entity>newArrayList();
        List<Entity> passengerEntities = Lists.<Entity>newArrayList();

        for(EntityTrackerEntry entitytrackerentry : this.entries) {
            Entity entity = entitytrackerentry.getTrackedEntity();
            if (entity.chunkCoordX == chunkIn.x && entity.chunkCoordZ == chunkIn.z) {
                for (EntityPlayerMP player : players) {
                    if (entity != player) {
                        entitytrackerentry.updatePlayerEntity(player);
                    }
                }

                if (entity instanceof EntityLiving && ((EntityLiving)entity).getLeashHolder() != null) {
                    leashedEntities.add(entity);
                }

                if (!entity.getPassengers().isEmpty()) {
                    passengerEntities.add(entity);
                }
            }
        }

        if (!leashedEntities.isEmpty()) {
            for(Entity entity1 : leashedEntities) {
                SPacketEntityAttach attachPacket = null;
                for (EntityPlayerMP player : players) {
                    if (entity1 == player) {
                        continue;
                    }
                    if (attachPacket == null) {
                        attachPacket = new SPacketEntityAttach(entity1, ((EntityLiving)entity1).getLeashHolder());
                    }
                    player.connection.sendPacket(attachPacket);
                }
            }
        }

        if (!passengerEntities.isEmpty()) {
            for(Entity entity2 : passengerEntities) {
                SPacketSetPassengers setPassengersPacket = null;
                for (EntityPlayerMP player : players) {
                    if (entity2 == player) {
                        continue;
                    }
                    if (setPassengersPacket == null) {
                        setPassengersPacket = new SPacketSetPassengers(entity2);
                    }
                    player.connection.sendPacket(setPassengersPacket);
                }
            }
        }
    }
}
