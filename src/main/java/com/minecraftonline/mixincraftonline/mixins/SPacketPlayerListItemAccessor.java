package com.minecraftonline.mixincraftonline.mixins;

import net.minecraft.network.play.server.SPacketPlayerListItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.List;

@Mixin(SPacketPlayerListItem.class)
public interface SPacketPlayerListItemAccessor {
    @Accessor
    public List<SPacketPlayerListItem.AddPlayerData> getPlayers();
}
