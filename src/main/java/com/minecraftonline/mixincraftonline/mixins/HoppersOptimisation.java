package com.minecraftonline.mixincraftonline.mixins;

import com.minecraftonline.mixincraftonline.MixincraftOnline;
import com.minecraftonline.mixincraftonline.bridge.EmptyOrFullInventoryCached;
import com.minecraftonline.mixincraftonline.bridge.HopperBridge;
import com.minecraftonline.mixincraftonline.util.BlockUtil;
import com.minecraftonline.mixincraftonline.util.InventoryCache;
import net.minecraft.block.BlockHopper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.IHopper;
import net.minecraft.tileentity.TileEntityHopper;
import net.minecraft.tileentity.TileEntityLockableLoot;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Event;
import org.spongepowered.api.event.SpongeEventFactory;
import org.spongepowered.api.event.item.inventory.ChangeInventoryEvent;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * <p>This mixin contains a number of optimisations
 * to make hoppers for efficient in terms of TPS</p>
 *
 * <p>This mixin completely overwrites the majority
 * of the hopper code, specifically, transfer and pull
 * methods, meaning that this mixin is highly incompatible.
 * <b>This mixin is not compatible with the base sponge.</b>
 * A fork of a sponge is required, disabling certain overwrites
 * and redirects is necessary. Thus, this mixin has priority 1.
 * <b>Furthermore, only {@link ChangeInventoryEvent.Transfer.Pre}</b>
 * is fired, and thus, if other plugins require listening to
 * these events, (for hoppers) they are out of luck until this
 * mixin is further updated.</p>
 *
 * <p>Optimisation point 1 - Remember which slots and items have previously
 * failed to fit items, or items which have failed to fit. This is done
 * by keeping an arraylist of previously checked items, so, in the case
 * that a hopper is full of cobblestone, but could not fit any from the
 * first slot, it will detect that the second slot will have an identical
 * result, and thus will skip that slot for the rest of the transfer/pull
 * attempt. Additionally, a boolean array, is used whenever a full slot
 * is encountered in the target inventory, to skip that slot, as it can
 * never receive any items. This will optimise in the case of a hopper
 * having multiple slots non-empty in its inventory, as the first slot
 * will fill the boolean array with slots that don't need to be checked
 * anymore, since they cannot ever fit any items. Potentially saved checking
 * the full slot 5 times, worst case, assuming the hopper is full</p>
 *
 * <p>Optimisation point 2 - Cache whether an inventory is full or empty.
 * A large amount of the tick was previously spent iterating through empty
 * hopper inventories to check they really were empty, or iterating through
 * full inventories to check they were really full. By storing the last result
 * of these emptiness/fullness checks, we avoid having to recalculate until
 * state has actually changed (The inventory has changed, or in our case,
 * when we are marked dirty, so some savable data has changed about us,
 * i.e inventory. This is currently implemented using {@link EmptyOrFullInventoryCached}.
 * This is currently implemented on hoppers and chests.</p>
 *
 * <p>Optimisation point 3 - Cache source and destination inventories
 * to avoid searching for tile entities above and where we are piping to.
 * We cache this in a {@link InventoryCache}, which is lazy and lazily initiated.
 * This is invalidated when our neighbour changes, and the neighbour is a source
 * or destination location. Check {@link HopperBlockInvalidateHopperCacheMixin} for details.</p>
 *
 * <p>Optimisation point 4 - Inline some functions to make to improve performance.</p>
 *
 * @author tyhdefu
 * @see HopperBlockInvalidateHopperCacheMixin
 */
@Mixin(value = TileEntityHopper.class, priority = 1)
public abstract class HoppersOptimisation extends TileEntityLockableLoot implements HopperBridge {
    @Shadow private static boolean isInventoryEmpty(IInventory inventoryIn, EnumFacing side) { throw new AbstractMethodError(); }
    @Shadow public static boolean putDropInInventoryAllSlots(IInventory source, IInventory destination, EntityItem entity) { throw new AbstractMethodError(); }
    @Shadow public static List<EntityItem> getCaptureItems(World worldIn, double p_184292_1_, double p_184292_3_, double p_184292_5_) { throw new AbstractMethodError(); }
    @Shadow protected abstract IInventory getInventoryForHopperTransfer();
    @Shadow protected abstract boolean isInventoryFull(IInventory inventoryIn, EnumFacing side);
    @Shadow public abstract int getSizeInventory();
    @Shadow public abstract ItemStack decrStackSize(int index, int count);
    @Shadow public abstract String getName();
    @Shadow protected abstract boolean isInventoryEmpty();
    @Shadow protected abstract boolean isFull();
    @Shadow private long tickedGameTime;
    @Shadow protected abstract boolean mayTransfer();
    @Shadow private int transferCooldown;
    @Shadow public abstract double getXPos();
    @Shadow public abstract double getYPos();
    @Shadow public abstract double getZPos();

    // Cache these until we get marked dirty.
    @Nullable
    private Boolean isEmptyCache = null;
    @Nullable
    private Boolean isFullCache = null;

    @Nullable
    private InventoryCache sourceCache = null;
    @Nullable
    private InventoryCache destCache = null;

    @Redirect(method = "updateHopper", at = @At(value = "INVOKE", target = "Lnet/minecraft/tileentity/TileEntityHopper;pullItems(Lnet/minecraft/tileentity/IHopper;)Z"))
    public boolean wrapPullItemsToSetNotEmpty(IHopper hopper) {
        if (pullItemsNonStatic()) {
            // Not empty, we just pulled items into ourself
            ((HopperBridge)hopper).bridge$setEmptyCache(false);
            return true;
        }
        return false;
    }

    public boolean pullItemsNonStatic() {
        if (this.sourceCache == null) {
            this.sourceCache = new InventoryCache(this.getWorld(), this.getPos().add(0, 1, 0));
        }
        IInventory iinventory = this.sourceCache.getOrLoad();

        if (iinventory != null)
        {
            EnumFacing enumfacing = EnumFacing.DOWN;

            // Empty of full cached inventories aren't sided.
            if (iinventory instanceof EmptyOrFullInventoryCached ? iinventory.isEmpty() : isInventoryEmpty(iinventory, enumfacing)) {
                return false;
            }
            if (iinventory instanceof ISidedInventory)
            {
                ISidedInventory isidedinventory = (ISidedInventory)iinventory;
                int[] aint = isidedinventory.getSlotsForFace(enumfacing);
                List<ItemStack> cantFitItems = new ArrayList<>();

                boolean[] fullSlots = new boolean[this.getSizeInventory()];

                outer: for (int i : aint)
                {
                    ItemStack itemStack = iinventory.getStackInSlot(i);
                    if (!isidedinventory.canExtractItem(i, itemStack, enumfacing)) {
                        continue;
                    }
                    if (itemStack.isEmpty()) {
                        continue;
                    }
                    for (ItemStack checkedItem : cantFitItems) {
                        if (stacksEqualIgnoringAmount(checkedItem, itemStack)) {
                            continue outer;
                        }
                    }
                    for (int j = 0; j < this.getSizeInventory(); j++) {
                        if (fullSlots[j]) {
                            continue;
                        }
                        ItemStack invSlot = this.getStackInSlot(j);
                        if (invSlot.isEmpty()) {
                            if (fireTransferPreEvent(this, this, iinventory)) {
                                return false;
                            }
                            this.setInventorySlotContents(j, iinventory.decrStackSize(i, 1));
                            iinventory.markDirty();
                            return true;
                        }
                        if (invSlot.getMaxStackSize() == invSlot.getCount()) {
                            fullSlots[j] = true;
                            continue;
                        }
                        if (stacksEqualIgnoringAmount(invSlot, itemStack)) {
                            if (fireTransferPreEvent(this, this, iinventory)) {
                                return false;
                            }
                            invSlot.grow(1);
                            iinventory.decrStackSize(i, 1);
                            iinventory.markDirty();
                            return true;
                        }
                    }
                    cantFitItems.add(itemStack.copy());
                }
            }
            else
            {
                int invSize = iinventory.getSizeInventory();

                List<ItemStack> cantFitItems = new ArrayList<>();

                boolean[] fullSlots = new boolean[this.getSizeInventory()];

                outer: for (int i = 0; i < invSize; ++i)
                {
                    ItemStack itemStack = iinventory.getStackInSlot(i);
                    if (itemStack.isEmpty()) {
                        continue;
                    }
                    for (ItemStack checkedItem : cantFitItems) {
                        if (stacksEqualIgnoringAmount(checkedItem, itemStack)) {
                            continue outer;
                        }
                    }
                    for (int j = 0; j < this.getSizeInventory(); j++) {
                        if (fullSlots[j]) {
                            continue;
                        }
                        ItemStack invSlot = this.getStackInSlot(j);
                        if (invSlot.isEmpty()) {
                            if (fireTransferPreEvent(this, this, iinventory)) {
                                return false;
                            }
                            this.setInventorySlotContents(j, iinventory.decrStackSize(i, 1));
                            iinventory.markDirty();
                            return true;
                        }
                        if (invSlot.getMaxStackSize() == invSlot.getCount()) {
                            fullSlots[j] = true;
                            continue;
                        }
                        if (stacksEqualIgnoringAmount(invSlot, itemStack)) {
                            if (fireTransferPreEvent(this, this, iinventory)) {
                                return false;
                            }
                            invSlot.grow(1);
                            iinventory.decrStackSize(i, 1);
                            iinventory.markDirty();
                            return true;
                        }
                    }
                    cantFitItems.add(itemStack.copy());
                }
            }
        }
        else
        {
            for (EntityItem entityitem : getCaptureItems(this.world, this.getXPos(), this.getYPos(), this.getZPos()))
            {
                if (putDropInInventoryAllSlots((IInventory)null, this, entityitem))
                {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean stacksEqualIgnoringAmount(ItemStack itemStack1, ItemStack itemStack2) {
        if (!itemStack1.isItemEqual(itemStack2)) {
            return false;
        }
        if (itemStack1.getTagCompound() == null) {
            return itemStack2.getTagCompound() == null;
        }
        return itemStack2.getTagCompound() != null
                && itemStack1.getTagCompound().equals(itemStack2.getTagCompound());
    }

    /**
     * @reason Huge amount of rewrites.
     * @author tyhdefu
     */
    @Overwrite
    private boolean transferItemsOut() {
        if (this.destCache == null) {
            this.destCache = new InventoryCache(this.getWorld(), this.getPos().offset(BlockHopper.getFacing(getBlockMetadata())));
        }
        IInventory iinventory = this.destCache.getOrLoad();
        if (iinventory == null) {
            return false;
        }
        if (!this.getWorld().isBlockLoaded(this.destCache.getPos())) {
            System.out.println("Block " + this.destCache.getPos() + " is not loaded!!");
        }
        EnumFacing enumfacing = BlockHopper.getFacing(this.getBlockMetadata()).getOpposite();
        if (iinventory instanceof EmptyOrFullInventoryCached ? ((EmptyOrFullInventoryCached)iinventory).bridge$isFullUsingCache() : isInventoryFull(iinventory, enumfacing)) {
            return false;
        }

        List<ItemStack> checkedItems = new ArrayList<>();
        boolean[] fullSlots = new boolean[iinventory.getSizeInventory()];

        outer: for(int i = 0; i < this.getSizeInventory(); ++i) {
            ItemStack ourStack = this.getStackInSlot(i);
            if (ourStack.isEmpty()) {
                continue;
            }

            for (ItemStack checked : checkedItems) {
                if (stacksEqualIgnoringAmount(ourStack, checked)) {
                    continue outer;
                }
            }

            if (iinventory instanceof ISidedInventory) {
                int[] allowedSlots = ((ISidedInventory) iinventory).getSlotsForFace(enumfacing);
                for (int j : allowedSlots) {
                    if (!((ISidedInventory) iinventory).canInsertItem(j, ourStack, enumfacing)) {
                        continue;
                    }
                    if (tryToInsertItems(iinventory, ourStack, i, j, fullSlots)) {
                        return true;
                    }
                }
            }
            else {
                for (int j = 0; j < iinventory.getSizeInventory(); j++) {
                    if (tryToInsertItems(iinventory, ourStack, i, j, fullSlots)) {
                        return true;
                    }
                }
            }
            checkedItems.add(ourStack);
        }
        return false;
    }

    private boolean tryToInsertItems(IInventory destination, ItemStack ourStack, int sourceSlot, int targetSlot, boolean[] fullSlots) {
        if (fullSlots[targetSlot]) {
            return false; // Slot is full.
        }
        ItemStack itemStack = destination.getStackInSlot(targetSlot);
        if (itemStack.isEmpty()) {
            if (fireTransferPreEvent(this, this, destination)) {
                return false;
            }
            destination.setInventorySlotContents(targetSlot, decrStackSize(sourceSlot, 1));
            destination.markDirty();
            if (destination instanceof TileEntityHopper) {
                HopperBridge destHopper = (HopperBridge)destination;
                // Not empty - we just stuck an item in it.
                destHopper.bridge$setEmptyCache(false);
                if (!destHopper.bridge$mayTransfer()) {
                    int k = 0;
                    if (destHopper.bridge$getTickedGameTime() >= this.tickedGameTime) {
                        k = 1;
                    }

                    destHopper.bridge$setTransferCooldown(8 - k);
                }
            }
            return true;
        }
        if (itemStack.getMaxStackSize() == itemStack.getCount()) {
            fullSlots[targetSlot] = true;
            return false;
        }
        if (stacksEqualIgnoringAmount(ourStack, itemStack)) {
            if (fireTransferPreEvent(this, this, destination)) {
                return false;
            }
            ourStack.shrink(1);
            itemStack.grow(1);
            destination.markDirty();
            if (destination instanceof HopperBridge) {
                ((HopperBridge)destination).bridge$setEmptyCache(false);
                // Not empty.
            }
            return true;
        }
        return false;
    }

    private static boolean fireTransferPreEvent(Object cause, IInventory source, IInventory target) {
        return Sponge.getEventManager().post(createTransferPreEvent(cause, source, target));
    }

    private static Event createTransferPreEvent(Object cause, IInventory source, IInventory target) {
        return SpongeEventFactory.createChangeInventoryEventTransferPre(
                Sponge.getCauseStackManager().getCurrentCause().with(cause),
                (Inventory) source,
                (Inventory) target);
    }

    public boolean impl$isOtherInventoryFull(IInventory iinventory, EnumFacing side) {
        if (iinventory instanceof EmptyOrFullInventoryCached) {
            return ((EmptyOrFullInventoryCached)iinventory).bridge$isFullUsingCache();
        }
        return isInventoryFull(iinventory, side);
    }

    private static boolean impl$isOtherInventoryEmpty(IInventory iinventory, EnumFacing side) {
        if (iinventory instanceof EmptyOrFullInventoryCached) {
            return iinventory.isEmpty();
        }
        return isInventoryEmpty(iinventory, side);
    }

    @Override
    public void markDirty() {
        super.markDirty();
        this.isEmptyCache = false;
        this.isFullCache = false;
    }

    /**
     * @reason Cache empty state.
     * We're unlikely to be compatible with anything here anyway.
     * @author tyhdefu
     */
    @Overwrite
    public boolean isEmpty() {
        if (this.isEmptyCache == null) {
            this.isEmptyCache = this.isInventoryEmpty();
        }
        return this.isEmptyCache;
    }

    @Redirect(method = "updateHopper", at = @At(value = "INVOKE", target = "Lnet/minecraft/tileentity/TileEntityHopper;isInventoryEmpty()Z"))
    public boolean redirectIsInventoryEmptyToIsEmpty(TileEntityHopper tileEntityHopper) {
        return this.isEmpty();
    }

    @Redirect(method = "updateHopper", at = @At(value = "INVOKE", target = "Lnet/minecraft/tileentity/TileEntityHopper;isFull()Z"))
    public boolean redirect$isFullToUseCache(TileEntityHopper ignored) {
        return this.bridge$isFullUsingCache();
    }

    // Implement bridge methods.

    @Override
    public boolean bridge$isFullUsingCache() {
        if (this.isFullCache == null) {
            this.isFullCache = this.isFull();
        }
        return this.isFullCache;
    }

    @Override
    public boolean bridge$mayTransfer() {
        return this.mayTransfer();
    }

    @Override
    public long bridge$getTickedGameTime() {
        return this.tickedGameTime;
    }

    @Override
    public void bridge$setTransferCooldown(int cooldown) {
        this.transferCooldown = cooldown;
    }

    @Override
    public void bridge$setEmptyCache(Boolean emptyCache) {
        this.isEmptyCache = emptyCache;
    }

    @Override
    public IInventory bridge$getCachedSourceInv() {
        if (this.sourceCache == null) {
            this.sourceCache = new InventoryCache(this.getWorld(), this.getPos().add(0, 1, 0));
        }
        return this.sourceCache.getOrLoad();
    }

    @Override
    public IInventory bridge$getCachedDestInv() {
        if (this.destCache == null) {
            this.destCache = new InventoryCache(this.getWorld(), this.getPos().offset(BlockHopper.getFacing(getBlockMetadata())));
        }
        return this.destCache.getOrLoad();
    }

    @Override
    public void bridge$invalidateSourceInv() {
        if (this.sourceCache != null) {
            this.sourceCache.invalidate();
        }
    }

    @Override
    public void bridge$invalidateDestInv() {
        if (destCache != null) {
            this.destCache.invalidate();
        }
    }
}
