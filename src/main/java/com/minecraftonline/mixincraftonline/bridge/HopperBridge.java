package com.minecraftonline.mixincraftonline.bridge;

import net.minecraft.inventory.IInventory;

public interface HopperBridge extends EmptyOrFullInventoryCached {

    long bridge$getTickedGameTime();

    boolean bridge$mayTransfer();

    void bridge$setTransferCooldown(int cooldown);

    IInventory bridge$getCachedSourceInv();

    IInventory bridge$getCachedDestInv();

    void bridge$invalidateSourceInv();

    void bridge$invalidateDestInv();
}
