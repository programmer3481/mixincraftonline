package com.minecraftonline.mixincraftonline.bridge;

public interface EmptyOrFullInventoryCached {

    boolean bridge$isFullUsingCache();

    void bridge$setEmptyCache(Boolean emptyCache);
}
